# frozen_string_literal: true

require 'simplecov'
require 'simplecov-cobertura'
require 'simplecov-lcov'

module SimpleCovEnv
  extend self

  def start!
    configure_profile
    configure_formatter

    SimpleCov.start
  end

  def configure_profile
    SimpleCov.configure do
      load_profile 'test_frameworks'
      track_files '{lib,triage}/**/*.rb'

      add_filter '/bin/'
      add_filter '/vendor/ruby/'

      merge_timeout 365 * 24 * 3600
    end
  end

  def configure_formatter
    SimpleCov::Formatter::LcovFormatter.config.report_with_single_file = true

    SimpleCov.formatters = SimpleCov::Formatter::MultiFormatter.new([
      SimpleCov::Formatter::SimpleFormatter,
      SimpleCov::Formatter::HTMLFormatter,
      SimpleCov::Formatter::CoberturaFormatter,
      SimpleCov::Formatter::LcovFormatter
    ])
  end
end
