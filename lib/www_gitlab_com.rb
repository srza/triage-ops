# frozen_string_literal: true

require 'json'
require 'yaml'
require 'httparty'
require 'mini_cache'

class WwwGitLabCom
  WWW_GITLAB_COM_SITE = 'https://about.gitlab.com'
  WWW_GITLAB_COM_SECTIONS_JSON = "#{WWW_GITLAB_COM_SITE}/sections.json"
  WWW_GITLAB_COM_STAGES_JSON = "#{WWW_GITLAB_COM_SITE}/stages.json"
  WWW_GITLAB_COM_GROUPS_JSON = "#{WWW_GITLAB_COM_SITE}/groups.json"
  WWW_GITLAB_COM_CATEGORIES_JSON = "#{WWW_GITLAB_COM_SITE}/categories.json"
  WWW_GITLAB_COM_TEAM_YML = "#{WWW_GITLAB_COM_SITE}/company/team/team.yml"
  ROULETTE_JSON = 'https://gitlab-org.gitlab.io/gitlab-roulette/roulette.json'
  DISTRIBUTION_PROJECTS_YML = 'https://gitlab.com/gitlab-org/distribution/monitoring/-/raw/master/lib/data_sources/projects.yaml'
  DATA_CACHE_DEFAULT_EXPIRATION = 60 * 30 # 30 minutes

  def self.sections
    cache.get_or_set(:sections) do
      MiniCache::Data.new(fetch_json(WWW_GITLAB_COM_SECTIONS_JSON), expires_in: DATA_CACHE_DEFAULT_EXPIRATION)
    end
  end

  def self.stages
    cache.get_or_set(:stages) do
      MiniCache::Data.new(fetch_json(WWW_GITLAB_COM_STAGES_JSON), expires_in: DATA_CACHE_DEFAULT_EXPIRATION)
    end
  end

  def self.groups
    cache.get_or_set(:groups) do
      MiniCache::Data.new(fetch_json(WWW_GITLAB_COM_GROUPS_JSON), expires_in: DATA_CACHE_DEFAULT_EXPIRATION)
    end
  end

  def self.categories
    cache.get_or_set(:categories) do
      MiniCache::Data.new(fetch_json(WWW_GITLAB_COM_CATEGORIES_JSON), expires_in: DATA_CACHE_DEFAULT_EXPIRATION)
    end
  end

  def self.team_from_www
    cache.get_or_set(:team_from_www) do
      data = fetch_yml(WWW_GITLAB_COM_TEAM_YML).each_with_object({}) do |item, memo|
        memo.merge!({ item['gitlab'] => item })
      end
      MiniCache::Data.new(data, expires_in: DATA_CACHE_DEFAULT_EXPIRATION)
    end
  end

  def self.roulette
    cache.get_or_set(:roulette) do
      MiniCache::Data.new(fetch_json(ROULETTE_JSON), expires_in: DATA_CACHE_DEFAULT_EXPIRATION)
    end
  end

  def self.distribution_projects
    cache.get_or_set(:distribution_projects) do
      MiniCache::Data.new(fetch_yml(DISTRIBUTION_PROJECTS_YML), expires_in: DATA_CACHE_DEFAULT_EXPIRATION)
    end
  end

  def self.fetch_json(json_url)
    json = with_retries { HTTParty.get(json_url, format: :plain) }
    JSON.parse(json)
  end
  private_class_method :fetch_json

  def self.fetch_yml(yaml_url)
    YAML.load(HTTParty.get(yaml_url))
  end
  private_class_method :fetch_yml

  def self.with_retries(attempts: 3)
    yield
  rescue Errno::ECONNRESET, OpenSSL::SSL::SSLError, Net::OpenTimeout
    retry if (attempts -= 1) > 0
    raise
  end
  private_class_method :with_retries

  def self.cache
    @cache ||= MiniCache::Store.new
  end
  private_class_method :cache
end
