# frozen_string_literal: true

require_relative '../appsec_processor'

module Triage
  class AppSecApprovalLabelAdded < AppSecProcessor
    react_to 'merge_request.update', 'merge_request.open'

    def applicable?
      super && appsec_approval_label_added?
    end

    def process
      if approved_by_appsec?
        discussion_id = add_to_appsec_thread(label_added_message)

        resolve_discussion(discussion_id)
      else
        # Make sure we unresolve as soon as possible
        if appsec_discussion_thread
          discussion_id = appsec_discussion_thread['id']
          unresolve_discussion(discussion_id)
        end

        add_to_appsec_thread(label_removed_message)
      end
    end

    def documentation
      <<~TEXT
        # TODO: Add actual documentation
      TEXT
    end

    private

    def appsec_approval_label_added?
      event.added_label_names.include?(APPSEC_APPROVAL_LABEL)
    end

    def label_added_message
      @label_added_message ||= unique_comment.wrap(<<~MARKDOWN.strip)
        :white_check_mark: AppSec added ~"#{APPSEC_APPROVAL_LABEL}" for
        #{event.last_commit_sha} by `@#{event.event_actor_username}`
      MARKDOWN
    end

    def label_removed_message
      @label_removed_message ||= unique_comment.wrap(<<~MARKDOWN.strip)
        @#{event.event_actor_username} Please don't add ~"#{APPSEC_APPROVAL_LABEL}"
        because it's supposed to be added by
        `@#{Triage::GITLAB_COM_APPSEC_GROUP}` or automation only.

        /unlabel ~"#{APPSEC_APPROVAL_LABEL}"
      MARKDOWN
    end
  end
end
