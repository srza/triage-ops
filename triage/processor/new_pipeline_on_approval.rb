# frozen_string_literal: true

require_relative '../job/trigger_pipeline_on_approval_job'
require_relative '../triage/processor'
require_relative '../triage/unique_comment'
require_relative '../triage/changes_checker'
require_relative 'label_jihu_contribution'

module Triage
  class NewPipelineOnApproval < Processor
    SKIP_WHEN_CHANGES_ONLY_REGEX = %r{\A(?:docs?|qa|\.gitlab/(issue|merge_request)_templates)/}
    UPDATE_GITALY_BRANCH = 'release-tools/update-gitaly'
    NewPipelineMessage = Struct.new(:event, :trigger_pipeline_automatically?) do
      def to_s
        <<~MARKDOWN.strip
          #{header}

          #{body}

          #{references}
        MARKDOWN
      end

      def header
        <<~HEADER.chomp
          :wave: `@#{event.event_actor_username}`, thanks for approving this merge request.
        HEADER
      end

      def body
        <<~BODY.chomp
          This is the first time the merge request is approved. #{call_to_action}
        BODY
      end

      def references
        <<~REF.chomp
          For more info, please refer to the following links:
          - [`rspec` minimal jobs](https://docs.gitlab.com/ee/development/pipelines.html#rspec-minimal-jobs)
          - [`jest` minimal jobs](https://docs.gitlab.com/ee/development/pipelines.html#jest-minimal-jobs)
          - [merging a merge request](https://docs.gitlab.com/ee/development/code_review.html#merging-a-merge-request).
        REF
      end

      def call_to_action
        if trigger_pipeline_automatically?
          'To ensure full test coverage, a new pipeline has been started.'
        else
          'To ensure full test coverage, please start a new pipeline before merging.'
        end
      end
    end

    react_to_approvals

    def applicable?
      event.from_gitlab_org? &&
        event.from_gitlab_org_gitlab? &&
        need_new_pipeline?(event) &&
        unique_comment.no_previous_comment?
    end

    def process
      trigger_pipeline_automatically =
        event.gitlab_org_author? || event.automation_author?

      message = unique_comment.wrap(
        NewPipelineMessage.new(event, trigger_pipeline_automatically))

      if trigger_pipeline_automatically
        trigger_merge_request_pipeline(message)
      else
        add_discussion(message)
      end
    end

    def documentation
      <<~TEXT
        # TODO: Add actual documentation
      TEXT
    end

    private

    def need_new_pipeline?(event)
      !event.source_branch_is?(UPDATE_GITALY_BRANCH) &&
        !event.target_branch_is_stable_branch? &&
        !changes_checker.only_change?(SKIP_WHEN_CHANGES_ONLY_REGEX)
    end

    def changes_checker
      @changes_checker ||= Triage::ChangesChecker.new(event.project_id, event.iid)
    end

    def unique_comment
      @unique_comment ||= UniqueComment.new(self.class.name, event)
    end

    def trigger_merge_request_pipeline(message)
      TriggerPipelineOnApprovalJob.perform_async(event.noteable_path, message)
    end
  end
end
